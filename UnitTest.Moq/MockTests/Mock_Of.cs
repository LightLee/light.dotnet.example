﻿using ConsoleApp.Moq.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest.Moq.MockTests
{
    /// <summary>
    /// Practice Class Moq.Mock's static Generic Method "Of"
    /// </summary>
    [TestClass()]
    public class Mock_Of
    {
        [TestMethod()]
        public void StubProperty_String()
        {
            var mock = new Mock<IFooHelper>();
            IFooHelper foo = Mock.Of<IFooHelper>(x =>
                x.Name == "name"
            );
            Assert.AreEqual(foo.Name, "name");
        }

        [TestMethod()]
        public void StubProperty_Object()
        {
            var bar = new Bar() { };
            IFooHelper foo = Mock.Of<IFooHelper>(x =>
                x.Bar == bar
            );

            Assert.AreEqual(foo.Bar, bar);
        }

        [TestMethod()]
        public void StubMethod_FixParam()
        {
            IFooHelper foo = Mock.Of<IFooHelper>(x =>
                x.DoSomething("aa") == true
            );
            Assert.IsTrue(foo.DoSomething("aa"));
            Assert.IsFalse(foo.DoSomething("bb"));
        }

        [TestMethod()]
        public void StubMethod_AnyParam()
        {
            IFooHelper foo =  Mock.Of<IFooHelper>(x =>
                x.DoSomething(It.IsAny<string>()) == true
            );
            Assert.IsTrue(foo.DoSomething("ab"));
        }

        [TestMethod()]
        public void StubMethod_NotNullParam()
        {
            IFooHelper foo = Mock.Of<IFooHelper>(x =>
               x.DoSomething(It.IsNotNull<string>()) == true
            );
            Assert.IsTrue(foo.DoSomething(""));
            Assert.IsFalse(foo.DoSomething(null));
        }

        [TestMethod()]
        public void StubMethod_RegexParam()
        {
            IFooHelper foo = Mock.Of<IFooHelper>(x =>
               x.DoSomething(It.IsRegex("a*b+c?")) == true
            );
            Assert.IsTrue(foo.DoSomething("abc"));
            Assert.IsTrue(foo.DoSomething("bc"));
            Assert.IsFalse(foo.DoSomething("aef"));
            Assert.IsFalse(foo.DoSomething("def"));
        }
        
    }
}