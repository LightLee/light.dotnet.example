﻿using ConsoleApp.Moq.EF.CodeFirst;
using ConsoleApp.Moq.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest.Moq.Tests
{
    /// <summary>
    /// Practice Class Moq.Mock's Methods "Setup" and "Return"
    /// </summary>
    [TestClass]
    public class Mock_SetupReturn
    {
        [TestMethod]
        public void MockDbContext()
        {
            var mockSet = new Mock<DbSet<Blogs>>();
            var mockContext = new Mock<BloggingEntities>();
            mockContext.Setup(m => m.Blogs).Returns(mockSet.Object);

            var service = new BlogService(mockContext.Object);
            service.AddBlog("ADO.NET Blog", "http://blogs.msdn.com/adonet");

            mockSet.Verify(m => m.Add(It.IsAny<Blogs>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

    }

}
