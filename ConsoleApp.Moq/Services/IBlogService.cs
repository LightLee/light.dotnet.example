﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ConsoleApp.Moq.EF.CodeFirst;

namespace ConsoleApp.Moq.Services
{
    public interface IBlogService
    {
        Blogs AddBlog(string name, string url);
        List<Blogs> GetAllBlogs();
        Task<List<Blogs>> GetAllBlogsAsync();
    }
}