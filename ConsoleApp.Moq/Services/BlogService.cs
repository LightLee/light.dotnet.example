﻿using ConsoleApp.Moq.EF.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Moq.Services
{
    public class BlogService : IBlogService
    {
        private BloggingEntities _context;

        public BlogService(BloggingEntities context)
        {
            _context = context;
        }

        public Blogs AddBlog(string name, string url)
        {
            var blog = _context.Blogs.Add(new Blogs { Name = name, Url = url });
            _context.SaveChanges();

            return blog;
        }

        public List<Blogs> GetAllBlogs()
        {
            var query = from b in _context.Blogs
                        orderby b.Name
                        select b;

            return query.ToList();
        }

        public async Task<List<Blogs>> GetAllBlogsAsync()
        {
            var query = from b in _context.Blogs
                        orderby b.Name
                        select b;

            return await query.ToListAsync();
        }
    }

}
